# -*- coding: UTF-8 -*-
#脚本中所有的windows路径需要替换成本机路径
#根据测试环境不同，修改教师端登录地址 line32
import os
import re
from selenium import webdriver
import win32api
import win32con
import datetime
import time
import pyperclip
from pymouse import PyMouse
import subprocess
import logging
from PIL import Image
from PIL import ImageGrab
# t = str(time.time())
#切换鼠标焦点ALT+TAB
def switchMouseFocus():
    win32api.keybd_event(18,0,0,0)
    win32api.keybd_event(9,0,0,0)
    time.sleep(0.2)
    win32api.keybd_event(18,0,win32con.KEYEVENTF_KEYUP,0)
    win32api.keybd_event(9,0,win32con.KEYEVENTF_KEYUP,0)
#打开教师端的网址
def teacherClient():
    global chromedriver
    chromedriver = (r"C:\Users\thinkpad\Downloads\chromedriver.exe")
    os.environ["webdriver.chrome.driver"] = chromedriver
    global driver
    driver = webdriver.Chrome(chromedriver)
    driver.get("http://localhost:4030/teacher.html#/login")
    logger.info("open teacher client")
#教师端登录输入用户名密码点登录
def login():
    # 获取username并赋值
    driver.find_element_by_id("input_001").send_keys("gaokun")

    # 获取password并赋值
    driver.find_element_by_id("input_002").send_keys("mp")
    # 点击OK按钮登录
    driver.find_element_by_tag_name("button").click()
    logger.info("login teacher client")
    time.sleep(1)
#添加问题类型1 问题 文字 答案 文字
def addQuestionType1():
    logger.info("try to add type1 question")
    driver.find_element_by_tag_name("button").click()
    time.sleep(0.5)
    questionTextLists = driver.find_elements_by_tag_name("input")
    for x in questionTextLists:
        x.send_keys("aaa")
    time.sleep(1)
    questionRadios = driver.find_elements_by_tag_name("md-radio-button")
#选项A为正确答案
    questionRadios[0].click()
#点击ok 确认添加问题
    driver.find_elements_by_class_name("md-button")[1].click()
    logger.info("finished to add type1 question")
#添加问题类型2 问题 图片文字 答案 文字
def addQuestionType2():
    logger.info("try to add type2 question")
    driver.find_element_by_tag_name("button").click()
    time.sleep(0.5)
    questionTextLists = driver.find_elements_by_tag_name("input")
    for x in questionTextLists:
        x.send_keys("aaa")
    questionPicLists = driver.find_elements_by_class_name("fa-plus")
    questionPicLists[0].click()
    pyperclip.copy("C:\Users\\thinkpad\Desktop\QQ20170105164702.jpg")
    time.sleep(1)
    #CTRL
    win32api.keybd_event(17,0,0,0)
    #V
    win32api.keybd_event(86,0,0,0)
    time.sleep(1)

    win32api.keybd_event(17,0,win32con.KEYEVENTF_KEYUP,0)
    win32api.keybd_event(86,0,win32con.KEYEVENTF_KEYUP,0)
    time.sleep(0.5)
    #ENTER
    win32api.keybd_event(13,0,0,0)
    time.sleep(0.5)
    win32api.keybd_event(13,0,win32con.KEYEVENTF_KEYUP,0)

    questionRadios = driver.find_elements_by_tag_name("md-radio-button")
#选项A为正确答案
    questionRadios[0].click()
#点击ok 确认添加问题
    driver.find_elements_by_class_name("md-button")[1].click()
    logger.info("finished to add type2 question")
#添加问题类型3 问题 文字 答案 文字图片
def addQuestionType3():
    logger.info("try to add type3 question")
    driver.find_element_by_tag_name("button").click()
    time.sleep(0.5)
    questionTextLists = driver.find_elements_by_tag_name("input")
    questionTextLists[0].send_keys("aaa")
    questionPicLists = driver.find_elements_by_class_name("fa-plus")
    del questionPicLists[0]
    for y in questionPicLists:
        y.click()
        pyperclip.copy("C:\Users\\thinkpad\Desktop\QQ20170105164702.jpg")
        time.sleep(1)
    #CTRL
        win32api.keybd_event(17,0,0,0)
    #V
        win32api.keybd_event(86,0,0,0)
        time.sleep(1)

        win32api.keybd_event(17,0,win32con.KEYEVENTF_KEYUP,0)
        win32api.keybd_event(86,0,win32con.KEYEVENTF_KEYUP,0)
        time.sleep(0.5)
    #ENTER
        win32api.keybd_event(13,0,0,0)
        time.sleep(0.5)
        win32api.keybd_event(13,0,win32con.KEYEVENTF_KEYUP,0)

    questionRadios = driver.find_elements_by_tag_name("md-radio-button")
#选项A为正确答案
    questionRadios[0].click()
#点击ok 确认添加问题
    driver.find_elements_by_class_name("md-button")[1].click()
    logger.info("finished to add type3 question")
#编辑第i个问题
def questionEdit(i):
    logger.info("try to add edit question"+str(i))
    #得到add和所有编辑按钮
    questionEditLists = driver.find_elements_by_class_name("btn")[1::2]
    #删除add按钮 得到编辑按钮的list
    del questionEditLists[len(questionEditLists)-1]
    questionEditLists.pop()
    #编辑第i个问题
    questionEditLists[i-1].click()
    time.sleep(0.5)
    questionEditLists = driver.find_elements_by_tag_name("input")
    for x in questionEditLists:
        x.clear()
        x.send_keys(u"zww")
    #修改正确答案的选项为C

    questionRadios = driver.find_elements_by_tag_name("md-radio-button")
    questionRadios[2].click()

    #点击cancel按钮
    # driver.find_elements_by_class_name("md-button")[1].click()
    #点击OK按钮
    driver.find_elements_by_class_name("md-button")[1].click()
    time.sleep(1)
    logger.info("finished to add edit question")
def questionEdit():
    logger.info("try to add edit question")
    #得到add和所有编辑按钮
    questionEditLists = driver.find_elements_by_class_name("btn")[1::2]
    #删除add按钮 得到编辑按钮的list
    del questionEditLists[len(questionEditLists)-1]
    #编辑第最后一个问题
    questionEditLists[len(questionEditLists)-1].click()
    time.sleep(0.5)
    questionEditLists = driver.find_elements_by_tag_name("input")
    for x in questionEditLists:
        x.clear()
        x.send_keys(u"三")
    #修改正确答案的选项为C

    questionRadios = driver.find_elements_by_tag_name("md-radio-button")
    questionRadios[2].click()

    #点击cancel按钮
    # driver.find_elements_by_class_name("md-button")[1].click()
    #点击OK按钮
    driver.find_elements_by_class_name("md-button")[1].click()
    time.sleep(1)
    logger.info("finished to add edit question")
#删除所有问题
def questionDel():
    logger.info("try to add delete question")
    #得到删除add和删除按钮的list
    questionDelLists = driver.find_elements_by_class_name("btn")[::2]
    #删除add按钮 得到只有删除按钮的list
    del questionDelLists[0]

    for x in questionDelLists:
        x.click()
        driver.find_elements_by_class_name("md-button")[1].click()
        time.sleep(1)
    logger.info("finished to add delete question")
# questionDel()
#执行windows上的bat文件
def runSikuliScript(path):
    logger.info("try to run sikuli script")
    filepath = path
    p = subprocess.Popen(filepath,shell=True,stdout=subprocess.PIPE)
    stdout,stderr = p.communicate()
    logger.info("finished to run sikuli script")
    #bat文件的路径
#判断绘本是否加载完成
def bookLoaded():
    #第一页的sentence显示表示绘本加载完成
    if (driver.find_elements_by_class_name("ng-binding")[9].text):
        return 1
    else:
        return 0
#教师端绘本翻页函数
def changePage():
    global totalPage
    # # if(bookLoaded()):
    #     #点击下一页
    #     # x = driver.find_element_by_css_selector("fa-step-forward")
    #     #上一页按钮
    #     previousPageBtn = driver.find_element_by_class_name("fa-step-backward")
    #     #下一页按钮
    nextPageBtn = driver.find_element_by_class_name("fa-step-forward")
        #当前绘本在第几页

        # 绘本共有多少页

    totalPage = int(driver.find_elements_by_class_name("ng-binding")[8].text)
    nextPageBtn.click()
    return totalPage
        #每0.5秒翻一页
    #     for x in range(1, totalPage):
    #         nextPageBtn.click()
    #         time.sleep(0.5)
    #         #当前页
    #         currentPageNum = int(driver.find_elements_by_class_name("ng-binding")[7].text)
    #         logging.info(currentPageNum)
    # else:
    #     print "error"
    #     sys.exit()
#教师端推题 i推送第几题到学生端
def pushQuestion(i):
    logger.info("start function pushQuestion"+str(i))
    #得到push按钮的list
    btnInPageQTab = driver.find_elements_by_class_name("btn-thin")
    #在btn list中删除copylink按钮
    btnInPageQTab.pop()
    pushBtnLists = btnInPageQTab[1::2]
    #点击push按钮
    pushBtnLists[i-1].click()
    logger.info("push no."+str(i)+" question")
    logger.info("finsihed function pushQuestion"+str(i))
#教师端推送所有问题
def pushQuestion():
    logger.info("start function push all Questions")
    #得到push按钮的list
    btnInPageQTab = driver.find_elements_by_class_name("btn-thin")
    #在btn list中删除copylink按钮
    btnInPageQTab.pop()
    pushBtnLists = btnInPageQTab[1::2]
    #点击push按钮
    for x in pushBtnLists:
        x.click()
        time.sleep(1)
    logger.info("finsihed function push all Questions")
#教师端清除指定问题的回答统计 i从一开始
def clearResult(i):
    logger.info("start function clearResult")
    #得到clear result按钮的list
    btnInPageQTab = driver.find_elements_by_class_name("btn-thin")
    btnInPageQTab.pop()
    clearBtnLists = btnInPageQTab[::2]
    #点击clear result按钮
    clearBtnLists[i-1].click()
    logger.info("finsihed function clearResult")
#教师端清除所有问题的回答统计
def clearResult():
        logger.info("start function clear all Result")
        # 得到clear result按钮的list
        btnInPageQTab = driver.find_elements_by_class_name("btn-thin")
        btnInPageQTab.pop()
        clearBtnLists = btnInPageQTab[::2]
        # 点击clear result按钮
        for x in clearBtnLists:
            x.click()
        logger.info("finsihed function clear all Result")
def copyCurrentLessonLink():
    logger.info("start function copyCurrentLessonLink")
    time.sleep(0.5)
    btnInPageQTab = driver.find_elements_by_class_name("btn-thin")
    btnInPageQTab.pop().click()
    # #判断当前页面是question页面
    # if(currentPage()=="lesson detail question pattern"):
    #     btnInPageQTab = driver.find_elements_by_class_name("btn-thin")
    #     btnInPageQTab.pop().click()
    # # 判断当前页面是book页面
    # elif(currentPage()=="lesson detail lesson live page"):
    #     btnInPageQTab = driver.find_elements_by_class_name("btn-thin")
    #     btnInPageQTab.pop().click()
    # else:
    #     logger.warning("current page did not have copy link button")
    logger.info("finished function copyCurrentLessonLink")
def studentJoinLesson():
    studentwebsiteaddress = pyperclip.paste()

    #打开浏览器访问学生端lesson页面
    global studentDriver
    studentDriver = webdriver.Chrome(chromedriver)
    studentDriver.maximize_window()
    studentDriver.get(studentwebsiteaddress)
    time.sleep(0.5)
    studentQtitle = studentDriver.find_element_by_class_name("question-title").text
    teacherQtitle = driver.find_elements_by_class_name("title")[1].text
    #判断当前学生端的页面内容和教师端的页面内容显示一致
    if (studentQtitle==teacherQtitle):
        print "same page"
        return studentDriver
    else:
        print "not same page"
def currentPage():

    lessonDetailPattern = re.compile(ur'^https|http:\/\/\w*:\d*\/\w*.\w*#\/lesson/\d*$')
    lessonDetailQuestionPattern = re.compile(ur'^https|http:\/\/\w*:\d*\/\w*.\w*#\/lesson/\d*\/question$')
    lessonDetailLivePattern = re.compile(ur'^https|http:\/\/\w*:\d*\/\w*.\w*#\/lesson/\d*\/live$')
    teacherClientPattern = re.compile(ur'^https|http:\/\/\w*:\d*\/\\teacher.\w*#\/lesson/\d*\/live$')
    studentClientPattern = re.compile(ur'^https|http:\/\/\w*:\d*\/\\studnet.\w*#\/lesson/\d*$')
    if(driver.current_url=="http://localhost:4030/teacher.html#/book" or driver.current_url=="localhost:4030/teacher.html#/book"):
        return "book page"
    elif(driver.current_url=="http://localhost:4030/teacher.html#/lesson"):
        return "lesson page"
    elif(re.search(lessonDetailPattern,driver.current_url)):
        return "lesson detail lesson page"
    elif(re.search(lessonDetailQuestionPattern,driver.current_url)):
        print "lesson detail question page"
    elif(re.search(lessonDetailLivePattern,driver.current_url)):
        print "lesson detail live page"
    elif(re.search(teacherClientPattern,driver.current_url)):
        print "teacher"
    elif(re.search(studentClientPattern,driver.current_url)):
        print "student"
    else:
        print "no match"
#教师端LessonDetail页面开始课程
def startLesson():
    driver.find_element_by_class_name("btn-success").click()
#教师端LessonDetail页面结束课程
def endLesson():
    driver.find_element_by_class_name("btn-warning").click()
#绘本页面截图 只保存绘本大小的图片 browser教师端driver or 学生端driver str “teacher” or “student” str2 str(time.time())
def screenBookShot(broswer,str1,str2):
    broswer.save_screenshot(str1+str2+'.jpg')
    imgelement = broswer.find_element_by_id("gameCanvas")
    location = imgelement.location
    size = imgelement.size
    rangle = (int(location['x']), int(location['y']), int(location['x'] + size['width']), int(location['y'] + size['height']))
    i = Image.open(str1+str2+'.jpg')
    frame4 = i.crop(rangle)
    frame4.save(str1+str2+'.jpg')
#log
def openclassLog():
    global logger
    logger = logging.getLogger('openclass log')
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler('test.log')
    fh.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger
openclassLog()
#链接
sLoginPage = "https://s.openclass.iplayabc.com/teacher.html#/login"
sBookPage = "https://s.openclass.iplayabc.com/teacher.html#/book"
sLessonPage = "https://s.openclass.iplayabc.com/teacher.html#/lesson"
lLoginPage = "http://localhost:4030/teacher.html#/login"
#时间戳
thistime = str(datetime.datetime.now())
#绘本截图 页数固定需要修改
def bookscreen():
    for x in range(1,28):
        screenBookShot(driver, "teacher", str(time.time()))
        time.sleep(1)
        switchMouseFocus()
        time.sleep(1)
        screenBookShot(studentDriver, "student", str(time.time()))
        time.sleep(1)
        switchMouseFocus()
        time.sleep(1)
        changePage()
        time.sleep(1)
def screenShot(browser,str1,str2):
    browser.save_screenshot(str1+str2+'.jpg')
teacherClient()
login()
time.sleep(1)
#最大化当前浏览器
driver.maximize_window()
time.sleep(5)


